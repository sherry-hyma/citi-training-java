package Library;

import java.util.ArrayList;

public class Library {
    
    private ArrayList<Item> libraryCollections = new ArrayList<Item>();

    public ArrayList<Item> getItems() {
        return libraryCollections;
    }

    public void addItem(Item i) {
        libraryCollections.add(i);
    }

    public void removeItem(Item i) {
        libraryCollections.remove(i);
    }

    public void borrowItem(Item i) {
        if (i.getType() == "Periodical") {
            System.out.println("Periodical cannot be borrowed.");
        } else {
            if (i.getBorrowStatus()) {
                System.out.println(i.getTitle() + " has already borrowed by someone, failed to borrow.");
            } else {
                i.borrowItem();
                System.out.println("You have borrowed " + i.getTitle() + " successfully.");
            }
        }

        return;
    }

    @Override
    public String toString() {
        String str = "";
        for (int i=0; i<this.getItems().size(); i++) {
            str += this.getItems().get(i).toString();
            if (i < this.getItems().size() - 1) {
                str += "\n";
            }
        }
        return str;
    }
}