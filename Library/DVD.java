package Library;

public class DVD extends Item {

    private Integer length = null;
    
    public DVD(String title) {
        super(title, "DVD");
    }
 
    public DVD(String title, int length) {
        super(title, "DVD");
        this.length = length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Integer getLength() {
        return this.length;
    }
}