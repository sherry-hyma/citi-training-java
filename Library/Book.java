package Library;

public class Book extends Item {

    private int pageNum;
    private String author = null;

    public Book(String title, String author) {
        super(title, "Book");
        this.author = author;
    }

    public Book(String title) {
        super(title, "Book");
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageNum() {
        return this.pageNum;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return this.author;
    }

}