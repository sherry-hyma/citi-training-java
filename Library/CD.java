package Library;

public class CD extends Item {

    private Integer length = null;

    public CD(String title) {
        super(title, "CD");
    }

    public CD(String title, int length) {
        super(title, "CD");
        this.length = length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Integer getLength() {
        return this.length;
    }

}