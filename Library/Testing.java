package Library;

public class Testing {
    
    public static void main(String[] args) {
        Library library = new Library();
        Book book1 = new Book("Hello world");
        CD cd1 = new CD("Abc song", 35);
        Periodical periodical1 = new Periodical("Today's cat");
        Book book2 = new Book("Meow meow", "Eric");
        DVD dvd1 = new DVD("My trip");
        Book book3 = new Book("Bye World");
        CD cd2 = new CD("animal songs");

        library.addItem(book1);
        library.addItem(cd1);
        library.addItem(periodical1);
        library.addItem(book2);
        library.addItem(dvd1);
        library.addItem(book3);
        library.addItem(cd2);

        // borrowing
        System.out.println(library + "\n");

        library.borrowItem(book2);
        System.out.println(book2 + "\n");
        library.borrowItem(book2);
        System.out.println(book2 + "\n");


        library.borrowItem(periodical1);
        System.out.println(periodical1 + "\n");

        // removing
        library.removeItem(dvd1);
        System.out.println(library + "\n");
        

    }
}