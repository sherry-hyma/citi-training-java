package Library;

public abstract class Item {

    private String type;
    private String title;
    private boolean isBorrow = false;

    public Item(String title, String type) {
        this.title = title;
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public String getTitle() {
        return this.title;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void borrowItem() {
        this.isBorrow = true;
    }

    public void returnItem() {
        this.isBorrow = false;
    }

    public Boolean getBorrowStatus() {
        return this.isBorrow;
    }

    @Override
    public String toString() {
        return this.getType() + ": " + this.getTitle() + ", borrow state - " + this.getBorrowStatus();
    }
    
}