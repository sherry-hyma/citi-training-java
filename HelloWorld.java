public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("hello world");

        int myint = 0b101_01_00_1;
        long ccard = 32498239423049L;
        float myfloat = 3.4f;

        byte b1 = 120, b2 = 120;

        byte b3 = (byte)(b1 + b2); // result of the addition (i.e. 11) is int
        System.out.println(b3);
    }
}