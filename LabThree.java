public class LabThree {

    public static void main(String[] args) {
     
        String make = "Toyota";
        String model = "Prius";

        double engineSize = 1.8;
        byte gear = 5;

        short speed = (short)(gear*20);

        System.out.printf("The make is %s\n", make);
        System.out.printf("The model is %s\n", model);
        System.out.printf("The engine size is %f\n", engineSize);
        System.out.printf("The speed is %d\n", speed);

        // Lab 4

        if (engineSize <= 1.3) {
            System.out.printf("This car is not powerful");
        } else {
            System.out.printf("This car is powerful");
        }

        switch (gear) {
            case 1:
                System.out.printf("The speed of this car should be less than to 10 mph\n");
                break;
            case 5:
                System.out.printf("The speed of this car should be over 45 mph\n");
                break;
            default:
                System.out.printf("The speed of this car should be over ? mph\n");
                break;
        }
        //  else if (gear == 4) {
        //     System.out.printf("The speed of this car should be less than or equal to 45 mph");
        // } else if (gear == 3) {
        //     System.out.printf("The speed of this car should be less than or equal to 35 mph");
        // } else if (gear == 2) {
        //     System.out.printf("The speed of this car should be less than or equal to 25 mph");
        
        int[] arr = new int[10];
        System.out.println("Leap years (1900 - 2000) are:");
        int counter = 0;
        for (int i = 1900; i<=2000; i++) {        
            if (counter >= 10) {
                break;
            }
            if (i % 4 == 0) {
                // System.out.print(i + " ");
                arr[counter] = i;
                counter ++;
            }
        }
        // System.out.println("finished");
        for (int i = 0; i< arr.length; i++) {
            System.out.println(arr[i] + " ");
        }
    }
}