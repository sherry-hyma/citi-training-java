package Lab10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestString {

    public static void main(String[] args) throws IOException {
        // q2
        String str = "example.doc";
        str = "example.bak";
        System.out.println(str);

        // q3
        BufferedReader reader =  
                   new BufferedReader(new InputStreamReader(System.in)); 
         
        // Reading data using readLine 
        String name = reader.readLine();
        String name2 = reader.readLine();
        if (name.compareTo(name2) != 0) {
            if (name.compareTo(name2) < 0) {
                System.out.println(name + " " + name2);
            } else {
                System.out.println(name2 + " " + name);
            }
        } else {
            System.out.println("equal");
        }

        // q4
        String str2 = "the quick brown fox swallowed down the lazy chicken";
        int index = 0, count = 0;
        while(true) {
            index = str2.indexOf("ow", index);
            if (index > 0) {
                index ++;
                count ++;
            } else {
                break;
            }
        }
        System.out.println(count);

        //q5
        String str3 = "Live not on evil".toLowerCase();
        str3 = str3.replaceAll(" ", "");
        int i = 0, j = str3.length()-1;
        boolean isPalindrome = true;
        while (i <= j) {
            if (Character.compare(str3.charAt(i), str3.charAt(j)) != 0) {
                isPalindrome = false;
                break;
            }
            i++;
            j--;
        }
        System.out.println("is palindrome: " + isPalindrome);

        // part 2
        String pattern = "MM-dd-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        System.out.println(simpleDateFormat.format(new Date()));
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(simpleDateFormat.format(new Date()));
        simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        System.out.println(simpleDateFormat.format(new Date()));


    }
}