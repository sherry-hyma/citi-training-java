package Lab6;

public class TestAccount2 {
    
    public static void main(String[] args) {

        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

            Account.setInterestRate(0.25);
        for (int i=0; i<arrayOfAccounts.length; i++) {
            Account acc = new Account(names[i], amounts[i]);
            arrayOfAccounts[i] = acc;
            System.out.println(arrayOfAccounts[i].getName() + "'s account:");
            System.out.println("balance: " + arrayOfAccounts[i].getBalance());
            acc.addInterest();
            System.out.println("new balance: " + arrayOfAccounts[i].getBalance());
        }

        Account acc0 = arrayOfAccounts[0];
        System.out.println(acc0.withdraw(50));
        System.out.println(acc0.withdraw());
        System.out.println("new balance: " + arrayOfAccounts[0].getBalance());

    }
}