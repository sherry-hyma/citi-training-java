package Lab6;

public class TestAccount {
    
    public static void main(String[] args) {
        Account myAccount = new Account();
        myAccount.setName("Sherry");
        myAccount.setBalance(800000);
        myAccount.addInterest();

        System.out.println(myAccount.getName() + "'s account has a balance of "+ myAccount.getBalance());

        //Lab 7
                
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for (int i=0; i<arrayOfAccounts.length; i++) {
            Account acc = new Account();
            acc.setBalance(amounts[i]);
            acc.setName(names[i]);
            arrayOfAccounts[i] = acc;
            System.out.println(arrayOfAccounts[i].getName() + "'s account has a balance of "+ arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println(arrayOfAccounts[i].getName() + "'s account modified balance is "+ arrayOfAccounts[i].getBalance() + "\n");
        }
    }
}