package Lab6;

public class Account {

    private double balance;
    private String name;
    private static double interestRate;

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public Account() {
        this.name = "Sherry";
        this.balance = 50;
    }

    public double getBalance() {
        return this.balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addInterest() {
        this.balance *= 1 + getInterestRate();
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount) {
        if (this.getBalance() < amount) {
            return false;
        } else {
          this.setBalance(this.getBalance()-amount);  
          return true;
        }
    }

    public boolean withdraw() {
        double amt = 20;
        return this.withdraw(amt);
    }

}