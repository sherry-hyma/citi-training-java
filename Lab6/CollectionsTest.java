package Lab6;

import java.util.HashSet;
import java.util.Iterator;

public class CollectionsTest {
    
    public static void main(String[] args) {
        
        HashSet<Account> accounts = new HashSet<Account>();

        Account acc = new Account();
        Account acc2 = new Account("Eric", 544);
        Account acc3 = new Account("Gurp", 2000);
        accounts.add(acc);
        accounts.add(acc2);
        accounts.add(acc3);

        Account.setInterestRate(0.3);

        Iterator<Account> itr = accounts.iterator();
        while (itr.hasNext()) {
            Account i = itr.next();
            System.out.println("Name: " + i.getName() + ", balance: " + i.getBalance());
            i.addInterest();
            System.out.println("New balance: " + i.getBalance());
        }
        System.out.println("------------------------");
        for (Account a : accounts) {
            System.out.println("Name: " + a.getName() + ", balance: " + a.getBalance());
            a.addInterest();
            System.out.println("New balance: " + a.getBalance());
        }
        System.out.println("------------------------");
        accounts.forEach(a -> {
            System.out.println("Name: " + a.getName() + ", balance: " + a.getBalance());
            a.addInterest();
            System.out.println("New balance: " + a.getBalance());
        });
    }
}